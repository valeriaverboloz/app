## Getting started

This repo is dedicated for app image creation. In the end of the pipeline it will put an artifact to AWS Container Registry and on the next step it will be reused to create EC2 instance.

For the whole process was used the next technologies:
- Docker for Dockerfile
- Terraform for Iac
- Gitlab for CI and SVC
- AWS IAM, ECR, EC2, CloudWatch

## Init step

Please follow the steps below to init the repo. 
The 'init.bash' script will:
- Run new user creation
- Create new AWS Container Registry
- Attach to it 4 aws policies which we will use in the next step.
    1. AmazonEC2ContainerRegistryPowerUser
    2. AmazonEC2ContainerRegistryReadOnly
    3. EC2
    4. CloudWatch

To run init script you should already have another AWS User with attached IAM policies. To make sure which IAM policy would be enough you can check in aws_policies/policies_iam.json

git clone repo
bash init.bash

Once new AWS User created, please go to AWS Concole and generate AWS Credentials: IAM -> Users -> 'new_user' -> Security Credentials -> Create Access Key. 

Then create 2 new projects on GitLab and put your newly created secrets in variables for both of the projects: GitLab Projects -> 'Project' -> Settings -> CI/CD -> Variables -> Expand

Put there 3 below variables and their values. You might need to unmark "Protect variable" because exported variables to pipelines will be running on protected branches only.

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION

## Run pipeline

Once all variables added, you may proceed with the pipeline running. Go to your Project -> CI/CD -> Pipeline -> Run pipeline. Wait till it is done and go the AWS Container Registry. The name of the registry is "java-app" and the image version is "latest".

Now you can go to "java-infra" repo to proceed with the next steps.
