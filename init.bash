#!/bin/bash
USERNAME=
ACCOUNT=

if [ -z "$USERNAME" ]; then
    echo "Please put here aws username with IAM policy attached"
    read -p 'Username: ' USERNAME
fi
if [ -z "$ACCOUNT" ]; then
    echo "Please specify aws Account ID"
    read -p 'Account ID: ' ACCOUNT
fi

function try_create_policy() {
    account=$1
    username=$2
    policy_name=$3
    file=$4

    policy_arn=arn:aws:iam::$account:policy/$policy_name

    aws iam get-policy --policy-arn $policy_arn --no-cli-pager

    if [ $? -eq 0 ]; then
        echo "$policy_name policy already exists"
    else
        echo "Creating $policy_name policy"
        aws iam create-policy --policy-name $policy_name --policy-document $file --no-cli-pager
    fi
    aws iam attach-user-policy --policy-arn $policy_arn --user-name $username
    echo "Done"
}

function try_create_role() {
    policy_name=$3
    file=$4
    
    policy_arn=arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly

    aws iam list-attached-role-policies --role-name $policy_name --no-cli-pager

    if [ $? -eq 0 ]; then
        echo "$policy_name role already exists"
    else
        echo "Creating $policy_name role"
        aws iam create-role --role-name $policy_name --assume-role-policy-document file://assume_role.json --no-cli-pager    
    fi
    aws iam attach-role-policy --role-name $policy_name --policy-arn $policy_arn --no-cli-pager
    echo "Done"
}

function try_create_user() {
    username=$1
    aws iam get-user --user-name $username --no-cli-pager
    if [ $? -eq 0 ]; then
        echo "$username username already exist"
    else
        echo "Creating $username username"
        aws iam create-user --user-name $username  --no-cli-pager
    fi
    echo "Done"
}

try_create_user $USERNAME

aws iam attach-user-policy --policy-arn arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser --user-name $USERNAME

echo "Creating 'java-app' AWS Container Registry"
aws ecr create-repository --repository-name java-app --no-cli-pager

try_create_policy $ACCOUNT $USERNAME iam_test file://aws_policies/policies_iam.json
try_create_policy $ACCOUNT $USERNAME ec2_test file://aws_policies/policies_ec2.json
try_create_policy $ACCOUNT $USERNAME cloudwatch_test file://aws_policies/policies_cloudwatch.json
try_create_role $ACCOUNT $USERNAME ecr-role file://aws_policies/assume_role.json
