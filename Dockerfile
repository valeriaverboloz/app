
FROM maven:3.8-jdk-8 AS build
RUN apt-get update && apt-get install -y git
RUN git clone https://gitlab.com/cloud-devops-assignments/spring-boot-react-example.git

## Build App
RUN cd spring-boot-react-example && \
    mvn -f pom.xml clean package

FROM openjdk:8-jre-slim
COPY --from=build /spring-boot-react-example/target/react-and-spring-data-rest-0.0.1-SNAPSHOT.jar /app/react-and-spring-data-rest-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/app/react-and-spring-data-rest-0.0.1-SNAPSHOT.jar"]
